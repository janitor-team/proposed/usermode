# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Audrey Simons <asimons@redhat.com>, 2003, 2005.
# Bettina De Monti <bdemonti@redhat.it>, 2001.
# Dimitris Glezos <glezos@indifex.com>, 2011.
# dominique bribanick <chepioq@gmail.com>, 2011.
# Gauthier Ancelin <gauthier.ancelin@laposte.net>, 2007, 2008.
# Jérôme Fenal <jfenal@gmail.com>, 2012.
# Nathalie Scholz <EMAIL@ADDRESS>, 2001.
# Stephane Raimbault <stephane.raimbault@free.fr>, 2004.
# Thomas Canniot <mrtom@fedoraproject.org>, 2007, 2010.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-08-21 00:32+0200\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2015-03-21 02:44-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: French <trans-fr@lists.fedoraproject.org>\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Zanata 3.9.6\n"

#. 
#. * Copyright (C) 2001 Red Hat, Inc.
#. *
#. * This is free software; you can redistribute it and/or modify it
#. * under the terms of the GNU General Public License as published by
#. * the Free Software Foundation; either version 2 of the License, or
#. * (at your option) any later version.
#. *
#. * This program is distributed in the hope that it will be useful, but
#. * WITHOUT ANY WARRANTY; without even the implied warranty of
#. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#. * General Public License for more details.
#. *
#. * You should have received a copy of the GNU General Public License
#. * along with this program; if not, write to the Free Software
#. * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#. 
#. Just a dummy file for gettext, containing messages emmitted by various
#. * commonly-used PAM modules.
#: ../../po/../dummy.h:21
msgid "OK"
msgstr "OK "

#: ../../po/../dummy.h:22
msgid "Exit"
msgstr "Quitter"

#: ../../po/../dummy.h:23
msgid "Cancel"
msgstr "Annuler"

#: ../../po/../dummy.h:24
msgid "Run Unprivileged"
msgstr "Exécuter sans privilèges"

#: ../../po/../dummy.h:25
msgid "(current) UNIX password: "
msgstr "Mot de passe UNIX (actuel) : "

#: ../../po/../dummy.h:26
msgid "New UNIX password: "
msgstr "Nouveau mot de passe UNIX : "

#: ../../po/../dummy.h:27
msgid "Retype new UNIX password: "
msgstr "Confirmer le nouveau mot de passe UNIX : "

#: ../../po/../dummy.h:28
msgid "BAD PASSWORD: it's WAY too short"
msgstr "MOT DE PASSE INCORRECT : VRAIMENT trop court"

#: ../../po/../dummy.h:29
msgid "BAD PASSWORD: it is too short"
msgstr "MOT DE PASSE INCORRECT : trop court"

#: ../../po/../dummy.h:30
msgid "Password unchanged"
msgstr "Mot de passe inchangé"

#: ../../po/../dummy.h:31
msgid "Sorry, passwords do not match"
msgstr "Désolé, les mots de passe ne correspondent pas"

#: ../../po/../dummy.h:32
msgid "Launch system-wide configuration tools without a password."
msgstr "Démarrer les outils de configuration système sans mot de passe."

#: ../../po/../dummy.h:33
msgid "login: "
msgstr "Identifiant : "

#: ../../po/../dummy.h:34
msgid "Password: "
msgstr "Mot de passe : "

#: ../../po/../gsmclient.c:362
#, c-format
msgid "%s failed to connect to the session manager: %s\n"
msgstr ""
"%s n'a pas réussi à établir la connexion avec le gestionnaire de sessions : "
"%s\n"

#: ../../po/../pam-panel-icon.c:89
#, c-format
msgid "Failed to drop administrator privileges: %s"
msgstr "Impossible d'abandonner les privilèges d'administrateur : %s"

#: ../../po/../pam-panel-icon.c:98
#, c-format
msgid ""
"Failed to drop administrator privileges: pam_timestamp_check returned "
"failure code %d"
msgstr ""
"Impossible d'abandonner les privilèges d'administrateur : "
"pam_timestamp_check a renvoyé un code d'erreur %d"

#: ../../po/../pam-panel-icon.c:146
msgid ""
"You're currently authorized to configure system-wide settings (that affect "
"all users) without typing the administrator password again. You can give up "
"this authorization."
msgstr ""
"Vous êtes actuellement autorisé à configurer des paramètres pour l'ensemble "
"du système (qui touchent tous les utilisateurs) sans devoir saisir à nouveau "
"le mot de passe de l'administrateur. Vous pouvez renoncer à cette "
"autorisation."

#: ../../po/../pam-panel-icon.c:151 ../../po/../pam-panel-icon.c:178
msgid "Keep Authorization"
msgstr "Garder l'autorisation"

#: ../../po/../pam-panel-icon.c:154 ../../po/../pam-panel-icon.c:185
msgid "Forget Authorization"
msgstr "Abandonner l'autorisation"

#: ../../po/../pam-panel-icon.c:285
msgid "pam_timestamp_check is not setuid root"
msgstr "pam_timestamp_check n'est pas setuid root"

#: ../../po/../pam-panel-icon.c:289
msgid "no controlling tty for pam_timestamp_check"
msgstr "aucun tty de contrôle pour pam_timestamp_check"

#: ../../po/../pam-panel-icon.c:293
msgid "user unknown to pam_timestamp_check"
msgstr "utilisateur inconnu de pam_timestamp_check"

#: ../../po/../pam-panel-icon.c:297
msgid "permissions error in pam_timestamp_check"
msgstr "erreur de permissions dans pam_timestamp_check"

#: ../../po/../pam-panel-icon.c:301
msgid "invalid controlling tty in pam_timestamp_check"
msgstr "tty de contrôle non valide dans pam_timestamp_check"

#: ../../po/../pam-panel-icon.c:319
#, c-format
msgid "Error: %s\n"
msgstr "Erreur : %s\n"

#: ../../po/../pam-panel-icon.c:384
#, c-format
msgid "Failed to run command \"%s\": %s\n"
msgstr "Impossible d'exécuter la commande « %s » : %s\n"

#: ../../po/../pam-panel-icon.c:396
#, c-format
msgid "Failed to set IO channel nonblocking: %s\n"
msgstr "Impossible de paramétrer l'option de non blocage du canal d'E/S : %s\n"

#: ../../po/../pam-panel-icon.c:476
#, c-format
msgid "pam-panel-icon: failed to connect to session manager\n"
msgstr ""
"pam-panel-icon : impossible d'établir la connexion avec le gestionnaire de "
"sessions\n"

#: ../../po/../userhelper-messages.c:29
msgid "Information updated."
msgstr "Informations mises à jour."

#: ../../po/../userhelper-messages.c:32
msgid "The password you typed is invalid.\n"
"Please try again."
msgstr "Le mot de passe n'est pas valide.\n"
"Veuillez essayer à nouveau."

#: ../../po/../userhelper-messages.c:37
msgid ""
"One or more of the changed fields is invalid.\n"
"This is probably due to either colons or commas in one of the fields.\n"
"Please remove those and try again."
msgstr ""
"Un ou plusieurs des champs modifiés ne sont pas valides.\n"
"Cela peut être dû à des caractères deux-points ou virgule dans\n"
"l'un des champs. Veuillez les retirer et essayer à nouveau."

#: ../../po/../userhelper-messages.c:42
msgid "Password resetting error."
msgstr "Erreur de réinitialisation du mot de passe."

#: ../../po/../userhelper-messages.c:45
msgid "Some systems files are locked.\n"
"Please try again in a few moments."
msgstr ""
"Certains fichiers système sont verrouillés.\n"
"Veuillez essayer à nouveau un peu plus tard."

#: ../../po/../userhelper-messages.c:48
msgid "Unknown user."
msgstr "Utilisateur inconnu."

#: ../../po/../userhelper-messages.c:49
msgid "Insufficient rights."
msgstr "Droits insuffisants."

#: ../../po/../userhelper-messages.c:50
msgid "Invalid call to subprocess."
msgstr "Appel non valide à un sous-processus. "

#: ../../po/../userhelper-messages.c:53
msgid ""
"Your current shell is not listed in /etc/shells.\n"
"You are not allowed to change your shell.\n"
"Consult your system administrator."
msgstr ""
"Votre shell actuel ne figure pas dans le fichier /etc/shells.\n"
"Vous n'êtes pas autorisé à changer votre shell.\n"
"Consultez votre administrateur système."

#. well, this is unlikely to work, but at least we tried...
#: ../../po/../userhelper-messages.c:58
msgid "Out of memory."
msgstr "Mémoire insuffisante."

#: ../../po/../userhelper-messages.c:59
msgid "The exec() call failed."
msgstr "L'appel à exec() a échoué."

#: ../../po/../userhelper-messages.c:60
msgid "Failed to find selected program."
msgstr "Impossible de trouver le programme sélectionné."

#. special no-display dialog
#: ../../po/../userhelper-messages.c:62
msgid "Request canceled."
msgstr "Requête annulée."

#: ../../po/../userhelper-messages.c:63
msgid "Internal PAM error occured."
msgstr "Une erreur interne PAM est survenue."

#: ../../po/../userhelper-messages.c:64
msgid "No more retries allowed"
msgstr "Aucune nouvelle tentative n'est autorisée"

#: ../../po/../userhelper-messages.c:65
msgid "Unknown error."
msgstr "Erreur inconnue."

#. First entry is default
#: ../../po/../userhelper-messages.c:67
msgid "Unknown exit code."
msgstr "Code de sortie inconnu."

#: ../../po/../userhelper-wrap.c:455
msgid "Query"
msgstr "Interroger"

#: ../../po/../userhelper-wrap.c:514 ../../po/../userhelper.c:607
#, c-format
msgid "Authenticating as \"%s\""
msgstr "Authentification en tant que « %s »"

#: ../../po/../userhelper-wrap.c:533 ../../po/../userhelper-wrap.c:673
msgid "Error"
msgstr "Erreur"

#: ../../po/../userhelper-wrap.c:533
msgid "Information"
msgstr "Informations"

#: ../../po/../userhelper-wrap.c:698
msgid "Changing password."
msgstr "Modifier le mot de passe."

#: ../../po/../userhelper-wrap.c:700
msgid "Changing personal information."
msgstr "Changement des informations personnelles."

#: ../../po/../userhelper-wrap.c:703
msgid "Changing login shell."
msgstr "Changement du shell de connexion."

#: ../../po/../userhelper-wrap.c:707
#, c-format
msgid ""
"You are attempting to run \"%s\" which may benefit from administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"Vous essayez d'exécuter « %s » qui pourrait nécessiter des privilèges de "
"super-utilisateur. Des informations supplémentaires sont requises pour cette "
"opération."

#: ../../po/../userhelper-wrap.c:709
#, c-format
msgid ""
"You are attempting to run \"%s\" which requires administrative privileges, "
"but more information is needed in order to do so."
msgstr ""
"Vous essayez d'exécuter « %s » qui nécessite des privilèges de super-"
"utilisateur. Des informations supplémentaires sont requises pour cette "
"opération."

#: ../../po/../userhelper-wrap.c:713
msgid ""
"You are attempting to run a command which may benefit from administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"Vous essayez d'exécuter une commande qui pourrait nécessiter des privilèges "
"de super-utilisateur. Des informations supplémentaires sont requises pour "
"cette opération."

#: ../../po/../userhelper-wrap.c:715
msgid ""
"You are attempting to run a command which requires administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"Vous essayez d'exécuter une commande qui nécessite des privilèges de super-"
"utilisateur. Des informations supplémentaires sont requises pour cette "
"opération."

#: ../../po/../userhelper-wrap.c:752
msgid "_Run Unprivileged"
msgstr "Exécute_r sans privilèges"

#: ../../po/../userhelper-wrap.c:899
#, c-format
msgid "Pipe error.\n"
msgstr "Erreur de tube.\n"

#: ../../po/../userhelper-wrap.c:906
#, c-format
msgid "Cannot fork().\n"
msgstr "Erreur fork().\n"

#: ../../po/../userhelper-wrap.c:930
#, c-format
msgid "Can't set binary encoding on an IO channel: %s\n"
msgstr ""
"Impossible d'associer l'encodage binaire à un canal d'entrée/sortie : %s\n"

#: ../../po/../userhelper-wrap.c:999 ../../po/../userhelper-wrap.c:1003
#, c-format
msgid "dup2() error.\n"
msgstr "Erreur dup2().\n"

#: ../../po/../userhelper-wrap.c:1014
#, c-format
msgid "execl() error, errno=%d\n"
msgstr "Erreur execl(), errno=%d\n"

#: ../../po/../userhelper.c:589
#, c-format
msgid ""
"You are attempting to run \"%s\" which may benefit from administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"Vous essayez d'exécuter « %s » qui pourrait nécessiter des privilèges du "
"super-utilisateur. Des informations supplémentaires sont requises pour cette "
"opération."

#: ../../po/../userhelper.c:591
#, c-format
msgid ""
"You are attempting to run \"%s\" which requires administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"Vous essayez d'exécuter « %s » qui nécessite des privilèges du super-"
"utilisateur. Des informations supplémentaires sont requises pour cette "
"opération."

#: ../../po/../userhelper.c:595
#, c-format
msgid ""
"You are attempting to run a command which may benefit from\n"
"administrative privileges, but more information is needed in order to do so."
msgstr ""
"Vous essayez d'exécuter une commande qui pourrait nécessiter des privilèges\n"
"du super-utilisateur. Des informations supplémentaires sont requises pour "
"cette opération."

#: ../../po/../userhelper.c:597
#, c-format
msgid ""
"You are attempting to run a command which requires administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"Vous essayez d'exécuter une commande qui nécessite des privilèges du\n"
"super-utilisateur. Des informations supplémentaires sont requises pour cette "
"opération."

#: ../../po/../userhelper.c:2085
#, c-format
msgid "userhelper must be setuid root\n"
msgstr "userhelper doit être setuid root\n"

#. FIXME: print a warning here?
#: ../../po/../userhelper.c:2177
#, c-format
msgid ""
"Unable to open graphical window, and unable to find controlling terminal.\n"
msgstr ""
"Impossible d'ouvrir une fenêtre graphique et de trouver un terminal de "
"contrôle.\n"

#: ../../po/../userhelper.c:2230 ../../po/../userinfo.c:387
#: ../../po/../usermount.c:672 ../../po/../usermount.c:681
#: ../../po/../userpasswd.c:37
#, c-format
msgid "Unexpected command-line arguments\n"
msgstr "Arugments inattendus sur la ligne de commande\n"

#: ../../po/../userinfo.c:411
#, c-format
msgid "You don't exist.  Go away.\n"
msgstr "Identité inexistante. Quitter.\n"

#: ../userinfo.desktop.in.h:1
msgid "About Myself"
msgstr "Renseignements personnels"

#: ../userinfo.desktop.in.h:2
msgid "Change personal information"
msgstr "Changement des informations personnelles"

#: ../usermode.ui.h:1
msgid "User Information"
msgstr "Informations utilisateur"

#: ../usermode.ui.h:2
msgid "Full Name:"
msgstr "Nom et prénom :"

#: ../usermode.ui.h:3
msgid "Office:"
msgstr "Bureau :"

#: ../usermode.ui.h:4
msgid "Office Phone Number:"
msgstr "Téléphone bureau :"

#: ../usermode.ui.h:5
msgid "Home Phone Number:"
msgstr "Téléphone domicile :"

#: ../usermode.ui.h:6
msgid "Login Shell:"
msgstr "Shell de connexion :"

#: ../../po/../usermount.c:61
msgid "_Mount"
msgstr "_Monter"

#: ../../po/../usermount.c:62
msgid "Un_mount"
msgstr "Dé_monter"

#: ../../po/../usermount.c:150
msgid "Error loading list of file systems"
msgstr "Erreur lors du chargement de la liste des systèmes de fichiers"

#. Create the dialog.
#: ../../po/../usermount.c:151 ../../po/../usermount.c:525
#: ../../po/../usermount.c:537
msgid "User Mount Tool"
msgstr "Outil de montage de l'utilisateur"

#: ../../po/../usermount.c:306
msgid ""
"Are you sure you want to format this disk?  You will destroy any data it "
"currently contains."
msgstr ""
"Êtes-vous sûr de vouloir formater ce disque ? Vous détruiriez toutes les "
"données qu'il contient."

#: ../../po/../usermount.c:317
msgid "Perform a _low-level format."
msgstr "Effectuer un formatage de _bas niveau."

#: ../../po/../usermount.c:354
msgid "Select a _filesystem type to create:"
msgstr "Sélectionner un type de système de _fichiers à créer :"

#: ../../po/../usermount.c:524
msgid ""
"There are no filesystems which you are allowed to mount or unmount.\n"
"Contact your administrator."
msgstr ""
"Il n'y a pas de système de fichiers montable ou démontable par les "
"utilisateurs.\n"
"Contactez votre administrateur."

#. Create the other buttons.
#: ../../po/../usermount.c:555
msgid "_Format"
msgstr "_Formater"

#: ../../po/../usermount.c:591
msgid "Device"
msgstr "Périphérique"

#: ../../po/../usermount.c:597
msgid "Directory"
msgstr "Répertoire"

#: ../../po/../usermount.c:603
msgid "Filesystem"
msgstr "Système de fichiers"

#: ../../po/../usermount.c:651
#, c-format
msgid "The device '%s' can not be formatted."
msgstr "Le périphérique « %s » ne peut pas être formaté."

#: ../usermount.desktop.in.h:1
msgid "Disk Management"
msgstr "Gestion du disque"

#: ../usermount.desktop.in.h:2
msgid "Mount and unmount filesystems"
msgstr "Monter et démonter les systèmes de fichiers"

#: ../userpasswd.desktop.in.h:1
msgid "Password"
msgstr "Mot de passe"

#: ../userpasswd.desktop.in.h:2
msgid "Change your login password"
msgstr "Modifier votre mot de passe de connexion"
