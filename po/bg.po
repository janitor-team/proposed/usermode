# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Alexander Todorov <atodorov@redhat.com>, 2008.
# Dimitris Glezos <glezos@indifex.com>, 2011.
# Valentin Laskov <laskov@festa.bg>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-08-21 00:32+0200\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2015-03-21 02:43-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Bulgarian <trans-bg@lists.fedoraproject.org>\n"
"Language: bg\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 3.9.6\n"

#. 
#. * Copyright (C) 2001 Red Hat, Inc.
#. *
#. * This is free software; you can redistribute it and/or modify it
#. * under the terms of the GNU General Public License as published by
#. * the Free Software Foundation; either version 2 of the License, or
#. * (at your option) any later version.
#. *
#. * This program is distributed in the hope that it will be useful, but
#. * WITHOUT ANY WARRANTY; without even the implied warranty of
#. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#. * General Public License for more details.
#. *
#. * You should have received a copy of the GNU General Public License
#. * along with this program; if not, write to the Free Software
#. * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#. 
#. Just a dummy file for gettext, containing messages emmitted by various
#. * commonly-used PAM modules.
#: ../../po/../dummy.h:21
msgid "OK"
msgstr "Да"

#: ../../po/../dummy.h:22
msgid "Exit"
msgstr "Изход"

#: ../../po/../dummy.h:23
msgid "Cancel"
msgstr "Отказ"

#: ../../po/../dummy.h:24
msgid "Run Unprivileged"
msgstr "Изпълнение без привилегии"

#: ../../po/../dummy.h:25
msgid "(current) UNIX password: "
msgstr "(текуща) ЮНИКС парола: "

#: ../../po/../dummy.h:26
msgid "New UNIX password: "
msgstr "Нова ЮНИКС парола: "

#: ../../po/../dummy.h:27
msgid "Retype new UNIX password: "
msgstr "Повторете новата ЮНИКС парола: "

#: ../../po/../dummy.h:28
msgid "BAD PASSWORD: it's WAY too short"
msgstr "ЛОША ПАРОЛА: прекалено къса е"

#: ../../po/../dummy.h:29
msgid "BAD PASSWORD: it is too short"
msgstr "ЛОША ПАРОЛА: много е къса"

#: ../../po/../dummy.h:30
msgid "Password unchanged"
msgstr "Паролата не е променена"

#: ../../po/../dummy.h:31
msgid "Sorry, passwords do not match"
msgstr "Грешка, паролите не съвпадат"

#: ../../po/../dummy.h:32
msgid "Launch system-wide configuration tools without a password."
msgstr "Стартиране на инструменти за настройка на цялата система без парола."

#: ../../po/../dummy.h:33
msgid "login: "
msgstr "потребител: "

#: ../../po/../dummy.h:34
msgid "Password: "
msgstr "Парола: "

#: ../../po/../gsmclient.c:362
#, c-format
msgid "%s failed to connect to the session manager: %s\n"
msgstr "%s неуспех при свързването с мениджъра на сесията: %s\n"

#: ../../po/../pam-panel-icon.c:89
#, c-format
msgid "Failed to drop administrator privileges: %s"
msgstr "Неуспешна отмяна на административните права: %s"

#: ../../po/../pam-panel-icon.c:98
#, c-format
msgid ""
"Failed to drop administrator privileges: pam_timestamp_check returned "
"failure code %d"
msgstr ""
"Неуспешна отмяна на административните права: pam_timestamp_check върна код "
"на грешка %d"

#: ../../po/../pam-panel-icon.c:146
msgid ""
"You're currently authorized to configure system-wide settings (that affect "
"all users) without typing the administrator password again. You can give up "
"this authorization."
msgstr ""
"В момента сте оторизиран да настройвате системни параметри (това засяга "
"всички потребители) без да въведете отново администраторската парола. Може "
"да се откажете от тази привилегия."

#: ../../po/../pam-panel-icon.c:151 ../../po/../pam-panel-icon.c:178
msgid "Keep Authorization"
msgstr "Запазване оторизацията"

#: ../../po/../pam-panel-icon.c:154 ../../po/../pam-panel-icon.c:185
msgid "Forget Authorization"
msgstr "Отказване оторизацията"

#: ../../po/../pam-panel-icon.c:285
msgid "pam_timestamp_check is not setuid root"
msgstr "pam_timestamp_check не е setuid root"

#: ../../po/../pam-panel-icon.c:289
msgid "no controlling tty for pam_timestamp_check"
msgstr "няма контролно tty за pam_timestamp_check"

#: ../../po/../pam-panel-icon.c:293
msgid "user unknown to pam_timestamp_check"
msgstr "потребителят е непознат на pam_timestamp_check"

#: ../../po/../pam-panel-icon.c:297
msgid "permissions error in pam_timestamp_check"
msgstr "грешка при правата в pam_timestamp_check"

#: ../../po/../pam-panel-icon.c:301
msgid "invalid controlling tty in pam_timestamp_check"
msgstr "невалидно контролиращо tty за pam_timestamp_check"

#: ../../po/../pam-panel-icon.c:319
#, c-format
msgid "Error: %s\n"
msgstr "Грешка: %s\n"

#: ../../po/../pam-panel-icon.c:384
#, c-format
msgid "Failed to run command \"%s\": %s\n"
msgstr "Провал при стартиране командата\"%s\":%s\n"

#: ../../po/../pam-panel-icon.c:396
#, c-format
msgid "Failed to set IO channel nonblocking: %s\n"
msgstr "Неуспех при настройка на IO канала в неблокиращ режим: %s\n"

#: ../../po/../pam-panel-icon.c:476
#, c-format
msgid "pam-panel-icon: failed to connect to session manager\n"
msgstr "pam-panel-icon: неуспех при свързването с мениджъра на сесията\n"

#: ../../po/../userhelper-messages.c:29
msgid "Information updated."
msgstr "Информацията е обновена."

#: ../../po/../userhelper-messages.c:32
msgid "The password you typed is invalid.\n"
"Please try again."
msgstr "Паролата, която въведохте, е невалидна.\n"
"Моля опитайте отново."

#: ../../po/../userhelper-messages.c:37
msgid ""
"One or more of the changed fields is invalid.\n"
"This is probably due to either colons or commas in one of the fields.\n"
"Please remove those and try again."
msgstr ""
"Едно или повече от променените полета не е валидно.\n"
"Това вероятно се дължи на двоеточия или кавички в някое от тях.\n"
"Моля премахнете ги и опитайте отново."

#: ../../po/../userhelper-messages.c:42
msgid "Password resetting error."
msgstr "Грешка при промяна на паролата."

#: ../../po/../userhelper-messages.c:45
msgid "Some systems files are locked.\n"
"Please try again in a few moments."
msgstr "Някои системни файлове са заключени.\n"
"Моля опитайте след малко."

#: ../../po/../userhelper-messages.c:48
msgid "Unknown user."
msgstr "Непознат потребител."

#: ../../po/../userhelper-messages.c:49
msgid "Insufficient rights."
msgstr "Недостатъчни права."

#: ../../po/../userhelper-messages.c:50
msgid "Invalid call to subprocess."
msgstr "Невалидно извикване на подпроцес."

#: ../../po/../userhelper-messages.c:53
msgid ""
"Your current shell is not listed in /etc/shells.\n"
"You are not allowed to change your shell.\n"
"Consult your system administrator."
msgstr ""
"Вашия команден интерпретатор не е указан в /etc/shells.\n"
"Не Ви е разрешено да промените своя команден интерпретатор.\n"
"Консултирайте се със системния си администратор."

#. well, this is unlikely to work, but at least we tried...
#: ../../po/../userhelper-messages.c:58
msgid "Out of memory."
msgstr "Недостатъчно памет."

#: ../../po/../userhelper-messages.c:59
msgid "The exec() call failed."
msgstr "Неуспешно извикване на exec()."

#: ../../po/../userhelper-messages.c:60
msgid "Failed to find selected program."
msgstr "Избраната програма не може да бъде намерена."

#. special no-display dialog
#: ../../po/../userhelper-messages.c:62
msgid "Request canceled."
msgstr "Заявката прекратена."

#: ../../po/../userhelper-messages.c:63
msgid "Internal PAM error occured."
msgstr "Възникна вътрешна PAM грешка."

#: ../../po/../userhelper-messages.c:64
msgid "No more retries allowed"
msgstr "Не са позволени повече опити"

#: ../../po/../userhelper-messages.c:65
msgid "Unknown error."
msgstr "Неизвестна грешка."

#. First entry is default
#: ../../po/../userhelper-messages.c:67
msgid "Unknown exit code."
msgstr "Неизвестен код на изход."

#: ../../po/../userhelper-wrap.c:455
msgid "Query"
msgstr "Заявка"

#: ../../po/../userhelper-wrap.c:514 ../../po/../userhelper.c:607
#, c-format
msgid "Authenticating as \"%s\""
msgstr "Удостоверяване като \"%s\""

#: ../../po/../userhelper-wrap.c:533 ../../po/../userhelper-wrap.c:673
msgid "Error"
msgstr "Грешка"

#: ../../po/../userhelper-wrap.c:533
msgid "Information"
msgstr "Информация"

#: ../../po/../userhelper-wrap.c:698
msgid "Changing password."
msgstr "Промяна на паролата."

#: ../../po/../userhelper-wrap.c:700
msgid "Changing personal information."
msgstr "Промяна на личната информация."

#: ../../po/../userhelper-wrap.c:703
msgid "Changing login shell."
msgstr "Промяна на командния интерпретатор."

#: ../../po/../userhelper-wrap.c:707
#, c-format
msgid ""
"You are attempting to run \"%s\" which may benefit from administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"Опитвате се да стартирате \"%s\". Може да са полезни административни права, "
"но за това е нужна повече информация."

#: ../../po/../userhelper-wrap.c:709
#, c-format
msgid ""
"You are attempting to run \"%s\" which requires administrative privileges, "
"but more information is needed in order to do so."
msgstr ""
"Опитвате се да стартирате \"%s\". Необходими са административни права, но за "
"това е нужна повече информация."

#: ../../po/../userhelper-wrap.c:713
msgid ""
"You are attempting to run a command which may benefit from administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"Опитвате се да стартирате команда, която може да се възползва от "
"административни права, но за това е нужна повече информация."

#: ../../po/../userhelper-wrap.c:715
msgid ""
"You are attempting to run a command which requires administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"Опитвате се да стартирате команда, която изисква административни права, но "
"за това е нужна повече информация."

#: ../../po/../userhelper-wrap.c:752
msgid "_Run Unprivileged"
msgstr "_Изпълнение без привилегии"

#: ../../po/../userhelper-wrap.c:899
#, c-format
msgid "Pipe error.\n"
msgstr "Грешка в програмен канал.\n"

#: ../../po/../userhelper-wrap.c:906
#, c-format
msgid "Cannot fork().\n"
msgstr "Неуспешно извикване на fork().\n"

#: ../../po/../userhelper-wrap.c:930
#, c-format
msgid "Can't set binary encoding on an IO channel: %s\n"
msgstr "Не може да се зададе двоично кодиране на IO канала:%s\n"

#: ../../po/../userhelper-wrap.c:999 ../../po/../userhelper-wrap.c:1003
#, c-format
msgid "dup2() error.\n"
msgstr "Грешка при извикване на dup2().\n"

#: ../../po/../userhelper-wrap.c:1014
#, c-format
msgid "execl() error, errno=%d\n"
msgstr "Грешка %d при извикване на execl()\n"

#: ../../po/../userhelper.c:589
#, c-format
msgid ""
"You are attempting to run \"%s\" which may benefit from administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"Опитвате се да стартирате \"%s\". В случая от полза могат да бъдат\n"
"административни права, но за това е нужна повече информация."

#: ../../po/../userhelper.c:591
#, c-format
msgid ""
"You are attempting to run \"%s\" which requires administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"Опитвате се да стартирате \"%s\". За целта са необходими административни\n"
"права, но за това е нужна повече информация."

#: ../../po/../userhelper.c:595
#, c-format
msgid ""
"You are attempting to run a command which may benefit from\n"
"administrative privileges, but more information is needed in order to do so."
msgstr ""
"Опитвате се да стартирате команда, която може да се възползва\n"
"от административни права, но за това е нужна повече информация."

#: ../../po/../userhelper.c:597
#, c-format
msgid ""
"You are attempting to run a command which requires administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"Опитвате се да стартирате команда, която изисква административни\n"
"права, но за това е нужна повече информация."

#: ../../po/../userhelper.c:2085
#, c-format
msgid "userhelper must be setuid root\n"
msgstr "userhelper трябва да бъде setuid root\n"

#. FIXME: print a warning here?
#: ../../po/../userhelper.c:2177
#, c-format
msgid ""
"Unable to open graphical window, and unable to find controlling terminal.\n"
msgstr ""
"Не може да бъде отворен графичен прозорец, а не може да бъде намерен и "
"контролен терминал.\n"

#: ../../po/../userhelper.c:2230 ../../po/../userinfo.c:387
#: ../../po/../usermount.c:672 ../../po/../usermount.c:681
#: ../../po/../userpasswd.c:37
#, c-format
msgid "Unexpected command-line arguments\n"
msgstr ""

#: ../../po/../userinfo.c:411
#, c-format
msgid "You don't exist.  Go away.\n"
msgstr "Вие не съществувате.  Вървете си.\n"

#: ../userinfo.desktop.in.h:1
msgid "About Myself"
msgstr "За мен"

#: ../userinfo.desktop.in.h:2
msgid "Change personal information"
msgstr "Промяна на личната информация"

#: ../usermode.ui.h:1
msgid "User Information"
msgstr "Потребителска информация"

#: ../usermode.ui.h:2
msgid "Full Name:"
msgstr "Пълно име:"

#: ../usermode.ui.h:3
msgid "Office:"
msgstr "Офис:"

#: ../usermode.ui.h:4
msgid "Office Phone Number:"
msgstr "Служебен телефон:"

#: ../usermode.ui.h:5
msgid "Home Phone Number:"
msgstr "Домашен телефон:"

#: ../usermode.ui.h:6
msgid "Login Shell:"
msgstr "Команден интерпретатор:"

#: ../../po/../usermount.c:61
msgid "_Mount"
msgstr "_Монтиране"

#: ../../po/../usermount.c:62
msgid "Un_mount"
msgstr "Де_монтиране"

#: ../../po/../usermount.c:150
msgid "Error loading list of file systems"
msgstr "Грешка при зареждане списъка на файловите системи"

#. Create the dialog.
#: ../../po/../usermount.c:151 ../../po/../usermount.c:525
#: ../../po/../usermount.c:537
msgid "User Mount Tool"
msgstr "Потребителски инструмент за монтиране"

#: ../../po/../usermount.c:306
msgid ""
"Are you sure you want to format this disk?  You will destroy any data it "
"currently contains."
msgstr ""
"Наистина ли искате да форматирате този диск?  Така ще унищожите всички данни "
"които той съдържа."

#: ../../po/../usermount.c:317
msgid "Perform a _low-level format."
msgstr "Форматиране на _ниско ниво."

#: ../../po/../usermount.c:354
msgid "Select a _filesystem type to create:"
msgstr "Изберете тип на _файловата система:"

#: ../../po/../usermount.c:524
msgid ""
"There are no filesystems which you are allowed to mount or unmount.\n"
"Contact your administrator."
msgstr ""
"Няма файлови системи, разрешени за монтиране или демонтиране от Вас.\n"
"Свържете се със своя администратор."

#. Create the other buttons.
#: ../../po/../usermount.c:555
msgid "_Format"
msgstr "_Форматиране"

#: ../../po/../usermount.c:591
msgid "Device"
msgstr "Устройство"

#: ../../po/../usermount.c:597
msgid "Directory"
msgstr "Директория"

#: ../../po/../usermount.c:603
msgid "Filesystem"
msgstr "Файлова система"

#: ../../po/../usermount.c:651
#, c-format
msgid "The device '%s' can not be formatted."
msgstr "Устройството '%s' не може да бъде форматирано."

#: ../usermount.desktop.in.h:1
msgid "Disk Management"
msgstr "Управление на дискове"

#: ../usermount.desktop.in.h:2
msgid "Mount and unmount filesystems"
msgstr "Монтиране и демонтиране на файлови системи"

#: ../userpasswd.desktop.in.h:1
msgid "Password"
msgstr "Парола"

#: ../userpasswd.desktop.in.h:2
msgid "Change your login password"
msgstr "Промяна на паролата за вход"
