# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Dimitris Glezos <glezos@indifex.com>, 2011.
# Roman Maurer <roman@lugos.si>, 2002.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-08-21 00:32+0200\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2015-03-21 02:47-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sl\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || "
"n%100==4 ? 2 : 3);\n"
"X-Generator: Zanata 3.9.6\n"

#. 
#. * Copyright (C) 2001 Red Hat, Inc.
#. *
#. * This is free software; you can redistribute it and/or modify it
#. * under the terms of the GNU General Public License as published by
#. * the Free Software Foundation; either version 2 of the License, or
#. * (at your option) any later version.
#. *
#. * This program is distributed in the hope that it will be useful, but
#. * WITHOUT ANY WARRANTY; without even the implied warranty of
#. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#. * General Public License for more details.
#. *
#. * You should have received a copy of the GNU General Public License
#. * along with this program; if not, write to the Free Software
#. * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#. 
#. Just a dummy file for gettext, containing messages emmitted by various
#. * commonly-used PAM modules.
#: ../../po/../dummy.h:21
msgid "OK"
msgstr "V redu"

#: ../../po/../dummy.h:22
msgid "Exit"
msgstr "Izhod"

#: ../../po/../dummy.h:23
msgid "Cancel"
msgstr "Preklic"

#: ../../po/../dummy.h:24
msgid "Run Unprivileged"
msgstr "Teci neprilivigiran"

#: ../../po/../dummy.h:25
msgid "(current) UNIX password: "
msgstr "(trenutno) geslo za UNIX: "

#: ../../po/../dummy.h:26
msgid "New UNIX password: "
msgstr "Novo geslo za UNIX: "

#: ../../po/../dummy.h:27
msgid "Retype new UNIX password: "
msgstr "Vnovič vpišite novo geslo za UNIX: "

#: ../../po/../dummy.h:28
msgid "BAD PASSWORD: it's WAY too short"
msgstr "SLABO GESLO: je ZELO prekratko"

#: ../../po/../dummy.h:29
msgid "BAD PASSWORD: it is too short"
msgstr "SLABO GESLO: je prekratko"

#: ../../po/../dummy.h:30
msgid "Password unchanged"
msgstr "Geslo ni spremenjeno"

#: ../../po/../dummy.h:31
msgid "Sorry, passwords do not match"
msgstr "Žal se gesli ne ujemata"

#: ../../po/../dummy.h:32
msgid "Launch system-wide configuration tools without a password."
msgstr "Zaženi sistemska nastavitvena orodja brez gesla."

#: ../../po/../dummy.h:33
msgid "login: "
msgstr "prijava: "

#: ../../po/../dummy.h:34
msgid "Password: "
msgstr "Geslo: "

#: ../../po/../gsmclient.c:362
#, c-format
msgid "%s failed to connect to the session manager: %s\n"
msgstr ""

#: ../../po/../pam-panel-icon.c:89
#, c-format
msgid "Failed to drop administrator privileges: %s"
msgstr ""

#: ../../po/../pam-panel-icon.c:98
#, c-format
msgid ""
"Failed to drop administrator privileges: pam_timestamp_check returned "
"failure code %d"
msgstr ""

#: ../../po/../pam-panel-icon.c:146
msgid ""
"You're currently authorized to configure system-wide settings (that affect "
"all users) without typing the administrator password again. You can give up "
"this authorization."
msgstr ""

#: ../../po/../pam-panel-icon.c:151 ../../po/../pam-panel-icon.c:178
msgid "Keep Authorization"
msgstr ""

#: ../../po/../pam-panel-icon.c:154 ../../po/../pam-panel-icon.c:185
msgid "Forget Authorization"
msgstr ""

#: ../../po/../pam-panel-icon.c:285
msgid "pam_timestamp_check is not setuid root"
msgstr ""

#: ../../po/../pam-panel-icon.c:289
msgid "no controlling tty for pam_timestamp_check"
msgstr ""

#: ../../po/../pam-panel-icon.c:293
msgid "user unknown to pam_timestamp_check"
msgstr ""

#: ../../po/../pam-panel-icon.c:297
msgid "permissions error in pam_timestamp_check"
msgstr ""

#: ../../po/../pam-panel-icon.c:301
msgid "invalid controlling tty in pam_timestamp_check"
msgstr ""

#: ../../po/../pam-panel-icon.c:319
#, c-format
msgid "Error: %s\n"
msgstr "Error: %s\n"

#: ../../po/../pam-panel-icon.c:384
#, c-format
msgid "Failed to run command \"%s\": %s\n"
msgstr ""

#: ../../po/../pam-panel-icon.c:396
#, c-format
msgid "Failed to set IO channel nonblocking: %s\n"
msgstr ""

#: ../../po/../pam-panel-icon.c:476
#, c-format
msgid "pam-panel-icon: failed to connect to session manager\n"
msgstr ""

#: ../../po/../userhelper-messages.c:29
msgid "Information updated."
msgstr "Podatek osvežen."

#: ../../po/../userhelper-messages.c:32
msgid "The password you typed is invalid.\n"
"Please try again."
msgstr "Geslo, ki ste ga podali, je neveljavno.\n"
"Prosimo, poskusite znova."

#: ../../po/../userhelper-messages.c:37
msgid ""
"One or more of the changed fields is invalid.\n"
"This is probably due to either colons or commas in one of the fields.\n"
"Please remove those and try again."
msgstr ""
"Eno ali več spremenjenih polj je neveljavnih.\n"
"Verjetno je to zaradi vejic ali podpičij v enem od polj.\n"
"Prosimo, odstranite jih in poskusite znova."

#: ../../po/../userhelper-messages.c:42
msgid "Password resetting error."
msgstr "Napaka pri vnovičnem nastavljanju gesla."

#: ../../po/../userhelper-messages.c:45
msgid "Some systems files are locked.\n"
"Please try again in a few moments."
msgstr ""
"Nekatere sistemske datoteke so zaklenjene.\n"
"Prosimo, poskusite znova čez nekaj časa."

#: ../../po/../userhelper-messages.c:48
msgid "Unknown user."
msgstr "Neznani uporabnik."

#: ../../po/../userhelper-messages.c:49
msgid "Insufficient rights."
msgstr "Premalo pravic."

#: ../../po/../userhelper-messages.c:50
msgid "Invalid call to subprocess."
msgstr "Neveljavni klic podprocesa."

#: ../../po/../userhelper-messages.c:53
msgid ""
"Your current shell is not listed in /etc/shells.\n"
"You are not allowed to change your shell.\n"
"Consult your system administrator."
msgstr ""
"Vaša trenutna ukazna lupina ni navedena v /etc/shells.\n"
"Ni vam je dovoljeno spremeniti.\n"
"Posvetujte se s skrbnikom vašega sistema."

#. well, this is unlikely to work, but at least we tried...
#: ../../po/../userhelper-messages.c:58
msgid "Out of memory."
msgstr "Zmanjkalo pomnilnika."

#: ../../po/../userhelper-messages.c:59
msgid "The exec() call failed."
msgstr "Klic exec() ni uspel."

#: ../../po/../userhelper-messages.c:60
msgid "Failed to find selected program."
msgstr "Izbranega programa ni moč najti."

#. special no-display dialog
#: ../../po/../userhelper-messages.c:62
msgid "Request canceled."
msgstr ""

#: ../../po/../userhelper-messages.c:63
msgid "Internal PAM error occured."
msgstr "Prišlo je do notranje PAM napake."

#: ../../po/../userhelper-messages.c:64
msgid "No more retries allowed"
msgstr ""

#: ../../po/../userhelper-messages.c:65
msgid "Unknown error."
msgstr "Neznana napaka."

#. First entry is default
#: ../../po/../userhelper-messages.c:67
msgid "Unknown exit code."
msgstr "Neznana izhodna vrednost."

#: ../../po/../userhelper-wrap.c:455
msgid "Query"
msgstr "Poizvedba"

#: ../../po/../userhelper-wrap.c:514 ../../po/../userhelper.c:607
#, c-format
msgid "Authenticating as \"%s\""
msgstr ""

#: ../../po/../userhelper-wrap.c:533 ../../po/../userhelper-wrap.c:673
msgid "Error"
msgstr "Napaka"

#: ../../po/../userhelper-wrap.c:533
msgid "Information"
msgstr "Podatki"

#: ../../po/../userhelper-wrap.c:698
msgid "Changing password."
msgstr ""

#: ../../po/../userhelper-wrap.c:700
msgid "Changing personal information."
msgstr "Sprememba osebnih podatkov."

#: ../../po/../userhelper-wrap.c:703
msgid "Changing login shell."
msgstr "Sprememba prijavne lupine."

#: ../../po/../userhelper-wrap.c:707
#, c-format
msgid ""
"You are attempting to run \"%s\" which may benefit from administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"Skušate pognati \"%s\", ki lahko pridobi, če ga poženete kot skrbnik "
"sistema, a za to je treba več podatkov."

#: ../../po/../userhelper-wrap.c:709
#, c-format
msgid ""
"You are attempting to run \"%s\" which requires administrative privileges, "
"but more information is needed in order to do so."
msgstr ""
"Skušate pognati \"%s\", ki zahteva, da ga poženete kot skrbnik sistema, a za "
"to je treba več podatkov."

#: ../../po/../userhelper-wrap.c:713
msgid ""
"You are attempting to run a command which may benefit from administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"Skušate pognati ukaz, ki lahko pridobi, če ga poženete kot skrbnik sistema, "
"a za to je treba več podatkov."

#: ../../po/../userhelper-wrap.c:715
msgid ""
"You are attempting to run a command which requires administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"Skušate pognati ukaz, ki zahteva, da ga poženete kot skrbnik sistema, a za "
"to je treba več podatkov."

#: ../../po/../userhelper-wrap.c:752
msgid "_Run Unprivileged"
msgstr "_Teci neprilivigiran"

#: ../../po/../userhelper-wrap.c:899
#, c-format
msgid "Pipe error.\n"
msgstr "Napaka s cevjo.\n"

#: ../../po/../userhelper-wrap.c:906
#, c-format
msgid "Cannot fork().\n"
msgstr "Klic fork() ni mogoč.\n"

#: ../../po/../userhelper-wrap.c:930
#, c-format
msgid "Can't set binary encoding on an IO channel: %s\n"
msgstr ""

#: ../../po/../userhelper-wrap.c:999 ../../po/../userhelper-wrap.c:1003
#, c-format
msgid "dup2() error.\n"
msgstr "Napaka pri dup2().\n"

#: ../../po/../userhelper-wrap.c:1014
#, c-format
msgid "execl() error, errno=%d\n"
msgstr "Napaka pri execl(), errno=%d\n"

#: ../../po/../userhelper.c:589
#, c-format
msgid ""
"You are attempting to run \"%s\" which may benefit from administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"Skušate pognati \"%s\", ki lahko pridobi, če ga poženete kot skrbnik\n"
"sistema, a za to je treba več podatkov."

#: ../../po/../userhelper.c:591
#, c-format
msgid ""
"You are attempting to run \"%s\" which requires administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"Skušate pognati \"%s\", ki zahteva, da ga poženete kot skrbnik sistema,\n"
"a za to je treba več podatkov."

#: ../../po/../userhelper.c:595
#, c-format
msgid ""
"You are attempting to run a command which may benefit from\n"
"administrative privileges, but more information is needed in order to do so."
msgstr ""
"Skušate pognati ukaz, ki lahko pridobi, če ga poženete kot skrbnik\n"
"sistema, a za to je treba več podatkov."

#: ../../po/../userhelper.c:597
#, c-format
msgid ""
"You are attempting to run a command which requires administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"Skušate pognati ukaz, ki zahteva, da ga poženete kot skrbnik sistema,\n"
"a za to je treba več podatkov."

#: ../../po/../userhelper.c:2085
#, c-format
msgid "userhelper must be setuid root\n"
msgstr "userhelper mora biti setuid root\n"

#. FIXME: print a warning here?
#: ../../po/../userhelper.c:2177
#, c-format
msgid ""
"Unable to open graphical window, and unable to find controlling terminal.\n"
msgstr "Ni moč odpreti grafičnega okna in ni moč najti nadzornega terminala.\n"

#: ../../po/../userhelper.c:2230 ../../po/../userinfo.c:387
#: ../../po/../usermount.c:672 ../../po/../usermount.c:681
#: ../../po/../userpasswd.c:37
#, c-format
msgid "Unexpected command-line arguments\n"
msgstr ""

#: ../../po/../userinfo.c:411
#, c-format
msgid "You don't exist.  Go away.\n"
msgstr "Ne obstajate. Pojdite stran.\n"

#: ../userinfo.desktop.in.h:1
msgid "About Myself"
msgstr "O meni"

#: ../userinfo.desktop.in.h:2
msgid "Change personal information"
msgstr "Sprememba osebnih podatkov"

#: ../usermode.ui.h:1
msgid "User Information"
msgstr "Podatki o uporabniku"

#: ../usermode.ui.h:2
msgid "Full Name:"
msgstr "Polno ime:"

#: ../usermode.ui.h:3
msgid "Office:"
msgstr "Pisarna:"

#: ../usermode.ui.h:4
msgid "Office Phone Number:"
msgstr "Službeni telefon:"

#: ../usermode.ui.h:5
msgid "Home Phone Number:"
msgstr "Domači telefon:"

#: ../usermode.ui.h:6
msgid "Login Shell:"
msgstr "Prijavna lupina:"

#: ../../po/../usermount.c:61
msgid "_Mount"
msgstr "_Priklopi"

#: ../../po/../usermount.c:62
msgid "Un_mount"
msgstr "Od_klopi"

#: ../../po/../usermount.c:150
msgid "Error loading list of file systems"
msgstr ""

#. Create the dialog.
#: ../../po/../usermount.c:151 ../../po/../usermount.c:525
#: ../../po/../usermount.c:537
msgid "User Mount Tool"
msgstr "Orodje za uporabniški priklop"

#: ../../po/../usermount.c:306
msgid ""
"Are you sure you want to format this disk?  You will destroy any data it "
"currently contains."
msgstr ""
"Ste prepričani, da želite formatirati ta disk? Uničili boste vse podatke, ki "
"jih trenutno vsebuje."

#: ../../po/../usermount.c:317
msgid "Perform a _low-level format."
msgstr "Napravi _nizko-nivojski format."

#: ../../po/../usermount.c:354
msgid "Select a _filesystem type to create:"
msgstr "Izberite vrsto _datotečnega sistema, ki naj se ustvari:"

#: ../../po/../usermount.c:524
msgid ""
"There are no filesystems which you are allowed to mount or unmount.\n"
"Contact your administrator."
msgstr ""
"Ni datotečnih sistemov, ki jih smete priklopiti ali odklopiti.\n"
"Posvetujte se s skrbnikom sistema."

#. Create the other buttons.
#: ../../po/../usermount.c:555
msgid "_Format"
msgstr "_Formatiraj"

#: ../../po/../usermount.c:591
msgid "Device"
msgstr "Naprava"

#: ../../po/../usermount.c:597
msgid "Directory"
msgstr "Imenik"

#: ../../po/../usermount.c:603
msgid "Filesystem"
msgstr "Datotečni sistem"

#: ../../po/../usermount.c:651
#, c-format
msgid "The device '%s' can not be formatted."
msgstr ""

#: ../usermount.desktop.in.h:1
msgid "Disk Management"
msgstr "Upravljanje z diski"

#: ../usermount.desktop.in.h:2
msgid "Mount and unmount filesystems"
msgstr "Priklop in odklop datotečnih sistemov"

#: ../userpasswd.desktop.in.h:1
msgid "Password"
msgstr "Geslo"

#: ../userpasswd.desktop.in.h:2
msgid "Change your login password"
msgstr "Spremeni vaše prijavno geslo"
