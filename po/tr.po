# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Dimitris Glezos <glezos@indifex.com>, 2011.
# Egemen Metin Turan <mturan@aegee-ankara.org>, 2006, 2007.
# Hasan Alp İNAN <hasanalpinan@gmail.com>, 2011.
# Altan Unsal <altanunsal@gmail.com>, 2016. #zanata
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-08-21 00:32+0200\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2016-10-24 04:57-0400\n"
"Last-Translator: Altan Unsal <altanunsal@gmail.com>\n"
"Language-Team: Turkish (http://www.transifex.com/projects/p/fedora/language/"
"tr/)\n"
"Language: tr\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Zanata 3.9.6\n"

#. 
#. * Copyright (C) 2001 Red Hat, Inc.
#. *
#. * This is free software; you can redistribute it and/or modify it
#. * under the terms of the GNU General Public License as published by
#. * the Free Software Foundation; either version 2 of the License, or
#. * (at your option) any later version.
#. *
#. * This program is distributed in the hope that it will be useful, but
#. * WITHOUT ANY WARRANTY; without even the implied warranty of
#. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#. * General Public License for more details.
#. *
#. * You should have received a copy of the GNU General Public License
#. * along with this program; if not, write to the Free Software
#. * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#. 
#. Just a dummy file for gettext, containing messages emmitted by various
#. * commonly-used PAM modules.
#: ../../po/../dummy.h:21
msgid "OK"
msgstr "TAMAM"

#: ../../po/../dummy.h:22
msgid "Exit"
msgstr "Çık"

#: ../../po/../dummy.h:23
msgid "Cancel"
msgstr "İptal"

#: ../../po/../dummy.h:24
msgid "Run Unprivileged"
msgstr "Yetkisiz çalıştırma"

#: ../../po/../dummy.h:25
msgid "(current) UNIX password: "
msgstr "(şu anki) UNIX parolası: "

#: ../../po/../dummy.h:26
msgid "New UNIX password: "
msgstr "Yeni UNIX parolası:"

#: ../../po/../dummy.h:27
msgid "Retype new UNIX password: "
msgstr "Yeni UNIX parolanızı tekrarlayın: "

#: ../../po/../dummy.h:28
msgid "BAD PASSWORD: it's WAY too short"
msgstr "PAROLA KÖTÜ: çok kolay"

#: ../../po/../dummy.h:29
msgid "BAD PASSWORD: it is too short"
msgstr "PAROLA KÖTÜ: çok kısa"

#: ../../po/../dummy.h:30
msgid "Password unchanged"
msgstr "Parola değişmedi"

#: ../../po/../dummy.h:31
msgid "Sorry, passwords do not match"
msgstr "Maalesef parolalar eşleşmedi"

#: ../../po/../dummy.h:32
msgid "Launch system-wide configuration tools without a password."
msgstr "Sistem yapılandırma araçlarını parolasız çalıştır."

#: ../../po/../dummy.h:33
msgid "login: "
msgstr "giriş: "

#: ../../po/../dummy.h:34
msgid "Password: "
msgstr "Parola: "

#: ../../po/../gsmclient.c:362
#, c-format
msgid "%s failed to connect to the session manager: %s\n"
msgstr "%s oturum yöneticisi ile iletişime geçme başarısız oldu: %s\n"

#: ../../po/../pam-panel-icon.c:89
#, c-format
msgid "Failed to drop administrator privileges: %s"
msgstr "Administrator ayrıcalıkları iptal edilemedi: %s"

#: ../../po/../pam-panel-icon.c:98
#, c-format
msgid ""
"Failed to drop administrator privileges: pam_timestamp_check returned "
"failure code %d"
msgstr ""
"Administrator ayrıcalıkları iptal edilemedi: pam_timestamp_check hata kodu "
"döndürdü %d"

#: ../../po/../pam-panel-icon.c:146
msgid ""
"You're currently authorized to configure system-wide settings (that affect "
"all users) without typing the administrator password again. You can give up "
"this authorization."
msgstr ""
"Şu anda tekrar administrator şifrenizi yazmasanız da system-geneli (tüm "
"kullanıcıları etkileyen) ayarları değiştirmekle yetkilisiniz. Bu yetkiyi "
"bırakabilirsiniz."

#: ../../po/../pam-panel-icon.c:151 ../../po/../pam-panel-icon.c:178
msgid "Keep Authorization"
msgstr "Yetkilendirmeyi Koru"

#: ../../po/../pam-panel-icon.c:154 ../../po/../pam-panel-icon.c:185
msgid "Forget Authorization"
msgstr "Yetkilendirmeyi Unut"

#: ../../po/../pam-panel-icon.c:285
msgid "pam_timestamp_check is not setuid root"
msgstr "pam_timestamp_check root setuid değil"

#: ../../po/../pam-panel-icon.c:289
msgid "no controlling tty for pam_timestamp_check"
msgstr "pam_timestamp_check için denetleyici tty yok"

#: ../../po/../pam-panel-icon.c:293
msgid "user unknown to pam_timestamp_check"
msgstr "kullanıcı pam_timestamp_check tarafından bilinmiyor"

#: ../../po/../pam-panel-icon.c:297
msgid "permissions error in pam_timestamp_check"
msgstr "pam_timestamp_check 'de izin hatası"

#: ../../po/../pam-panel-icon.c:301
msgid "invalid controlling tty in pam_timestamp_check"
msgstr "pam_timestamp_check 'te geçersiz denetleyici tty"

#: ../../po/../pam-panel-icon.c:319
#, c-format
msgid "Error: %s\n"
msgstr "Hata: %s\n"

#: ../../po/../pam-panel-icon.c:384
#, c-format
msgid "Failed to run command \"%s\": %s\n"
msgstr "Komut çalıştırılamadı \"%s\": %s\n"

#: ../../po/../pam-panel-icon.c:396
#, c-format
msgid "Failed to set IO channel nonblocking: %s\n"
msgstr "Giriş/Çıkış kanalını tıkanmasız ayarlanamadı: %s\n"

#: ../../po/../pam-panel-icon.c:476
#, c-format
msgid "pam-panel-icon: failed to connect to session manager\n"
msgstr "pam-panel-icon: oturum yöneticisine bağlanılamadı\n"

#: ../../po/../userhelper-messages.c:29
msgid "Information updated."
msgstr "Bilgiler güncellendi."

#: ../../po/../userhelper-messages.c:32
msgid "The password you typed is invalid.\n"
"Please try again."
msgstr "Girilen parola geçersiz.\n"
"Lütfen tekrar deneyin."

#: ../../po/../userhelper-messages.c:37
msgid ""
"One or more of the changed fields is invalid.\n"
"This is probably due to either colons or commas in one of the fields.\n"
"Please remove those and try again."
msgstr ""
"Değiştirilen alanların bir ya da birden fazlası geçersiz.\n"
"Bu sorun alanlardaki iki nokta üstüsteler ya da virgüllerden dolayı olabilir."
"\n"
"Bunları silip yeniden deneyin."

#: ../../po/../userhelper-messages.c:42
msgid "Password resetting error."
msgstr "Parola sıfırlama hatası."

#: ../../po/../userhelper-messages.c:45
msgid "Some systems files are locked.\n"
"Please try again in a few moments."
msgstr "Bazı sistem dosyaları kilitli.\n"
"Daha sonra tekrar deneyiniz."

#: ../../po/../userhelper-messages.c:48
msgid "Unknown user."
msgstr "Bilinmeyen kullanıcı."

#: ../../po/../userhelper-messages.c:49
msgid "Insufficient rights."
msgstr "Yetersiz hak."

#: ../../po/../userhelper-messages.c:50
msgid "Invalid call to subprocess."
msgstr "Alt sürece hatalı çağrı."

#: ../../po/../userhelper-messages.c:53
msgid ""
"Your current shell is not listed in /etc/shells.\n"
"You are not allowed to change your shell.\n"
"Consult your system administrator."
msgstr ""
"Kullandığınız kabuk /etc/shells dosyasında bulunmuyor.\n"
"Kabuk değiştirmeniz de mümkün değil. Lütfen\n"
"sistem yöneticinize başvurun."

#. well, this is unlikely to work, but at least we tried...
#: ../../po/../userhelper-messages.c:58
msgid "Out of memory."
msgstr "Bellek yetersiz."

#: ../../po/../userhelper-messages.c:59
msgid "The exec() call failed."
msgstr "exec() çağrısı başarısız."

#: ../../po/../userhelper-messages.c:60
msgid "Failed to find selected program."
msgstr "Seçilen program bulunamadı."

#. special no-display dialog
#: ../../po/../userhelper-messages.c:62
msgid "Request canceled."
msgstr "İstek iptal edildi."

#: ../../po/../userhelper-messages.c:63
msgid "Internal PAM error occured."
msgstr "Dahili PAM hatası oluştu."

#: ../../po/../userhelper-messages.c:64
msgid "No more retries allowed"
msgstr "Daha fazla denemeye izin verilmedi"

#: ../../po/../userhelper-messages.c:65
msgid "Unknown error."
msgstr "Bilinmeyen hata."

#. First entry is default
#: ../../po/../userhelper-messages.c:67
msgid "Unknown exit code."
msgstr "Bilinmeyen çıkış kodu."

#: ../../po/../userhelper-wrap.c:455
msgid "Query"
msgstr "Sorgu"

#: ../../po/../userhelper-wrap.c:514 ../../po/../userhelper.c:607
#, c-format
msgid "Authenticating as \"%s\""
msgstr "\"%s\" olarak kimliği doğrulanıyor"

#: ../../po/../userhelper-wrap.c:533 ../../po/../userhelper-wrap.c:673
msgid "Error"
msgstr "Hata"

#: ../../po/../userhelper-wrap.c:533
msgid "Information"
msgstr "Bilgi"

#: ../../po/../userhelper-wrap.c:698
msgid "Changing password."
msgstr "Parola değişiyor."

#: ../../po/../userhelper-wrap.c:700
msgid "Changing personal information."
msgstr "Kişisel bilgiler değiştiriliyor."

#: ../../po/../userhelper-wrap.c:703
msgid "Changing login shell."
msgstr "Giriş kabuğu değiştiriliyor."

#: ../../po/../userhelper-wrap.c:707
#, c-format
msgid ""
"You are attempting to run \"%s\" which may benefit from administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"root yetkilerinden yaralanarak \"%s\"i çalıştırmaya çalışıyorsunuz ama bunu "
"yapmak için daha fazla bilgi gerekiyor."

#: ../../po/../userhelper-wrap.c:709
#, c-format
msgid ""
"You are attempting to run \"%s\" which requires administrative privileges, "
"but more information is needed in order to do so."
msgstr ""
"root yetkileri gerektiren \"%s\"i çalıştırmaya çalışıyorsunuz ama bunu "
"yapmak için daha fazla bilgi gerekiyor."

#: ../../po/../userhelper-wrap.c:713
msgid ""
"You are attempting to run a command which may benefit from administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"root yetkilerinden yaralanarak bir komut çalıştırmaya çalışıyorsunuz ama "
"bunu yapmak için daha fazla bilgi gerekiyor."

#: ../../po/../userhelper-wrap.c:715
msgid ""
"You are attempting to run a command which requires administrative "
"privileges, but more information is needed in order to do so."
msgstr ""
"root yetkileri gerektiren bir komut çalıştırmaya çalışıyorsunuz ama bunu "
"yapmak için daha fazla bilgi gerekiyor."

#: ../../po/../userhelper-wrap.c:752
msgid "_Run Unprivileged"
msgstr "_Yetkisiz Çalıştırma"

#: ../../po/../userhelper-wrap.c:899
#, c-format
msgid "Pipe error.\n"
msgstr "Veri yolu hatası.\n"

#: ../../po/../userhelper-wrap.c:906
#, c-format
msgid "Cannot fork().\n"
msgstr "fork() çalıştırılamadı.\n"

#: ../../po/../userhelper-wrap.c:930
#, c-format
msgid "Can't set binary encoding on an IO channel: %s\n"
msgstr "Bir IO yolunda ikili kodlama ayarlanamıyor: %s\n"

#: ../../po/../userhelper-wrap.c:999 ../../po/../userhelper-wrap.c:1003
#, c-format
msgid "dup2() error.\n"
msgstr "dup2() hatası.\n"

#: ../../po/../userhelper-wrap.c:1014
#, c-format
msgid "execl() error, errno=%d\n"
msgstr "execl() hatası, hatano=%d\n"

#: ../../po/../userhelper.c:589
#, c-format
msgid ""
"You are attempting to run \"%s\" which may benefit from administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"root yetkilerinden yaralanarak \"%s\"i çalıştırmaya çalışıyorsunuz ama\n"
"bunu yapmak için daha fazla bilgi gerekiyor."

#: ../../po/../userhelper.c:591
#, c-format
msgid ""
"You are attempting to run \"%s\" which requires administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"root yetkileri gerektiren \"%s\"i çalıştırmaya çalışıyorsunuz ama\n"
"bunu yapmak için daha fazla bilgi gerekiyor."

#: ../../po/../userhelper.c:595
#, c-format
msgid ""
"You are attempting to run a command which may benefit from\n"
"administrative privileges, but more information is needed in order to do so."
msgstr ""
"root yetkilerinden yaralanarak bir komut çalıştırmaya çalışıyorsunuz ama\n"
"bunu yapmak için daha fazla bilgi gerekiyor."

#: ../../po/../userhelper.c:597
#, c-format
msgid ""
"You are attempting to run a command which requires administrative\n"
"privileges, but more information is needed in order to do so."
msgstr ""
"root yetkileri gerektiren bir komut çalıştırmaya çalışıyorsunuz ama\n"
"bunu yapmak için daha fazla bilgi gerekiyor."

#: ../../po/../userhelper.c:2085
#, c-format
msgid "userhelper must be setuid root\n"
msgstr "userhelper programı setuid root olmalıdır.\n"

#. FIXME: print a warning here?
#: ../../po/../userhelper.c:2177
#, c-format
msgid ""
"Unable to open graphical window, and unable to find controlling terminal.\n"
msgstr "Grafik pencere açılamadı ve uçbirim denetimi bulunamadı.\n"

#: ../../po/../userhelper.c:2230 ../../po/../userinfo.c:387
#: ../../po/../usermount.c:672 ../../po/../usermount.c:681
#: ../../po/../userpasswd.c:37
#, c-format
msgid "Unexpected command-line arguments\n"
msgstr "Beklenmeyen komut satırı argümanları\n"

#: ../../po/../userinfo.c:411
#, c-format
msgid "You don't exist.  Go away.\n"
msgstr "Yok ol.  Defol.\n"

#: ../userinfo.desktop.in.h:1
msgid "About Myself"
msgstr "Hakkımda"

#: ../userinfo.desktop.in.h:2
msgid "Change personal information"
msgstr "Kişisel bilgileri değiştir"

#: ../usermode.ui.h:1
msgid "User Information"
msgstr "Kullanıcı Bilgisi"

#: ../usermode.ui.h:2
msgid "Full Name:"
msgstr "Tam İsim:"

#: ../usermode.ui.h:3
msgid "Office:"
msgstr "İş Yeri:"

#: ../usermode.ui.h:4
msgid "Office Phone Number:"
msgstr "İş Telefonu:"

#: ../usermode.ui.h:5
msgid "Home Phone Number:"
msgstr "Ev Telefonu:"

#: ../usermode.ui.h:6
msgid "Login Shell:"
msgstr "Sisteme Giriş Kabuğu:"

#: ../../po/../usermount.c:61
msgid "_Mount"
msgstr "_Bağla"

#: ../../po/../usermount.c:62
msgid "Un_mount"
msgstr "_Ayır"

#: ../../po/../usermount.c:150
msgid "Error loading list of file systems"
msgstr "Dosya sistemleri listesi yüklenirken hata oluştu"

#. Create the dialog.
#: ../../po/../usermount.c:151 ../../po/../usermount.c:525
#: ../../po/../usermount.c:537
msgid "User Mount Tool"
msgstr "Bağlama Aracı"

#: ../../po/../usermount.c:306
msgid ""
"Are you sure you want to format this disk?  You will destroy any data it "
"currently contains."
msgstr ""
"Bu diski gerçekten biçemlemek istiyor musunuz? Diskteki tüm veri silinecek."

#: ../../po/../usermount.c:317
msgid "Perform a _low-level format."
msgstr "_Düşük düzeyli biçimleme uygula."

#: ../../po/../usermount.c:354
msgid "Select a _filesystem type to create:"
msgstr "_Oluşturulacak dosya sistemi türünü seçiniz."

#: ../../po/../usermount.c:524
msgid ""
"There are no filesystems which you are allowed to mount or unmount.\n"
"Contact your administrator."
msgstr ""
"Kullanıcının bağlayabileceği ya da ayırabileceği dosya sistemi\n"
"bulunmuyor. Sistem yöneticinize başvurun."

#. Create the other buttons.
#: ../../po/../usermount.c:555
msgid "_Format"
msgstr "_Biçemle"

#: ../../po/../usermount.c:591
msgid "Device"
msgstr "Aygıt"

#: ../../po/../usermount.c:597
msgid "Directory"
msgstr "Dizin"

#: ../../po/../usermount.c:603
msgid "Filesystem"
msgstr "Dosya sistemi"

#: ../../po/../usermount.c:651
#, c-format
msgid "The device '%s' can not be formatted."
msgstr "'%s' aygıtı formatlanamaz."

#: ../usermount.desktop.in.h:1
msgid "Disk Management"
msgstr "Disk Yönetimi"

#: ../usermount.desktop.in.h:2
msgid "Mount and unmount filesystems"
msgstr "Dosya sistemlerini bağla ve kaldır"

#: ../userpasswd.desktop.in.h:1
msgid "Password"
msgstr "Parola"

#: ../userpasswd.desktop.in.h:2
msgid "Change your login password"
msgstr "Giriş parolasını değiştir"
